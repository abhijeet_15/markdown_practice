# All About Git

## What is Git?

Git is *distributed* version control system used for tracking versions of your files. It is a free and open-source software which helps programmers effectively collaborate while developing a software.

Git was created for use in the development of the Linux kernel by [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds) and others developing the kernel.


## Benefits of Git

1. Free and open-source. It has plenty of resources and large active community which helps to troubleshoot issues faster.

2. Facilicates developers to collaborate on the same project. t provides mechanisms for sharing code, reviewing changes, and managing conflicts that may arise when multiple developers modify the same files.

3. Allows branching and merging, that developers can create a branch of working source code and modify the files and then merge them back to working source code branch.

There are a lot of other benefits as well. But these are few of major benefits of using Git which a developer must be aware of.

## Some commonly used terms with Git

1. **Repository:** A repository, also called as "repo", is a collection of files and directories.

2. **Commit:** A commit represents a snapshot of the repository at a specific point in time. It records changes made to the repository files since the last commit.

3. **Branch:** A branch is like a copy of the main code base which developers create to make their modifications to the code so that main branch (master branch) remains unaltered till the modifications are complete and tested thoroughly.

4. **Merge:** Merging is the process of combing the changes from one into another (oftenly the master branch). Git performs automatic merging unless their coflicts in the code which requires manual intervention.

5. **Pull:** Pull refers to fetching the changes from the remote repositories (generally hosted on Git service providers like GitHub, GitLab, etc.) to your local sytem.

6. **Push:** Push refers to sending your commited changes to the remote repositores (generally hosted on Git service providers like GitHub, GitLab, etc.) from your local system.


## What is working directory, staging area (index), commit area?

1. **Working directory:** - It is the simply the directory currently working.

2. **Staging area (index):** - It is a place where you prepare and organize changes before committing them to the Git repository.

3. **Commit area:** - Once the files are added to staging area and if commit is made then the code is moved to the commit area. This gives the access to switch back the commit and fetch the version of the base while it was commited.

![Diagram of Git workflow in a project](https://media.dev.to/cdn-cgi/image/width=800%2Cheight=%2Cfit=scale-down%2Cgravity=auto%2Cformat=auto/https%3A%2F%2Fdev-to-uploads.s3.amazonaws.com%2Fuploads%2Farticles%2Fvpxeexqyfvf4hw3zxtbn.png)

## Basic Git commands

1. **Git init:** Initializes an empty git repository in the current directory.
    ```
    git init

2. **git add <file_name>:** To add files to the staging area (use . instead to add all files at once)
    ```
    git add <file_name>

3. **git commit:** To records the changes staged in the current branch. Every commit creates a new snapshot in the repository's history.
    ```
    git commit -m "Commit message"

4. **git status:** Shows the current status of the working directory and staging area. It displays which files are modified, staged, or untracked.
    ```
    git status

5. **git log:** Shows the commit history along with commit messages, author, date, and commit hashes.
    ```
    git log

6. **git branch:** Displays all the branches in the repository and also indicates the current branch with asterisk(*)
    ```
    git branch

7. **git branch <branch_name>:** Creates a new branch with the specified name
    ```
    git branch <branch_name>

8. **git checkout <branch_name>:** Switch to the specified branch name
    ```
    git checkout <branch_name>

9. **git pull:** Fetches changes from a remote repository and integrates them into the current branch. It is used to update your local repository with changes made by others.
    ```
    git pull

10. **git push:** Pushes committed code from your local repository to a remote repository. It updates the remote repository with your local changes.
    ```
    git push


