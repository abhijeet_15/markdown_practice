# Command Line Interface (CLI)

## What is Command Line Interface (CLI)?

A command line interface (CLI) is a software mechanism you use to interact with your operating system using your keyboard. Another mechnism which is widely used in all applications and software systems is the Graphical User Interface (GUI) where the user can visually navigate and click on icons and images to carry out the operations. 
With a command line interface, you can enter text commands to configure, navigate, or run programs on any server or computer system. All operating systems including Linux, macOS, and Windows provide a CLI for faster system interaction. However, using command line interface (CLI) could be a tedious if the user is not a technology enthusiast.

## What are the benefits of using CLI?

1. **Efficiency:** CLI commands often allow tasks to be performed more quickly compared to graphical interfaces, especially for repetitive or batch operations. Commands can be executed swiftly once mastered, reducing the time required to navigate menus or click through options.

2. **Remote Access:** CLI applications often consume fewer network resources compared to graphical applications. This makes CLI applications suitable for systems with limited hardware capabilities or remote server environments. You can manage servers remotely by submitting commands over a secure shell, even with low bandwidth connections. It’s a preferred method for managing servers and cloud instances, especially in headless environments where no graphical interface is available.

3. **Automation:** CLI commands are scriptable, meaning sequences of commands can be stored in scripts or batch files for automated execution. This capability is essential for tasks like system administration, batch processing, and deployment workflows.

4. **Troubleshooting:** For developers and system administrators, using command line interfaces is a valuable skill that encourages a deeper understanding of the underlying system. It can make you more proficient with various tools and utilities and improve error management.

5. **Collaboration with Version Control:** CLI integrates well with version control sytems like Git. Developers widely use CLI tools to interact with the remote repositories and can perform wide range of tasks allowing faster development.


## Where is CLI Used?

Command Line Interface (CLI) are used across a wide range of computing environments and domains due to their efficiency, flexibility, and automation capabilities. Here are some key areas where CLI usage is prominent:

1. **System Administration:** System administrators use CLIs to fix system-related issues, check the operating system configurations, and change or update configurations on remote machines. They can run commands and control systems at scale.

2. **Development and Programming:** Developers use CLIs for version control (e.g., Git), package management (e.g., npm, pip), compiling code, running tests, building automation scripts, interacting with development environments (e.g., Docker CLI), and debugging applications.

3. **Cloud Computing:** CLI tools are essential in managing cloud resources and services. Providers like AWS (Amazon Web Services), Google Cloud Platform (GCP), Microsoft Azure, and others offer CLI interfaces (e.g., AWS CLI, gcloud CLI, Azure CLI) for provisioning resources, configuring services, deploying applications, and automating cloud operations.

4. **Networking:** Network engineers and administrators use CLIs for configuring routers, switches, firewalls, and other network devices. CLIs provide direct access to network configurations, routing tables, monitoring tools (e.g., ping, traceroute), and troubleshooting utilities.

These are only a few examples of use cases of CLI. However, there a lot of other use cases of CLI like database management, security, scripting automation, etc.


## Working of Command Line Interface

A command line interface (CLI) is a text-based interface where you can input commands that interact with a computer’s operating system. The CLI operates with the help of the default shell, which is between the operating system and the user.

### CLI Shell

A shell refers to a program that is an intermediary between the user and the operating system. Examples include Bash, cmd.exe, PowerShell, etc.

The shell is responsible for handling various tasks such as command parsing, environment management, and process execution. Shells also support features like these:

- Command history, where you can access previously entered commands using arrow keys or other shortcuts
- Input and output redirection
- Piping — where you connect the output of one command as the input of another and environment variable management

### How commands work

When you enter a command in the CLI, the following steps occur: 

1. The shell command line interpreter parses the entered command to understand its structure and separate the command name, options, and arguments.
2. The shell looks up the command name in its list of available commands. The command name represents the action the user wants the operating system to perform.
3. It searches the system's PATH variable (a list of directories where system files are located) to find the corresponding file associated with the command.
4. The CLI shell calls the appropriate file passing any specified options and arguments as input.
5. The operating system performs the required actions.
6. It may produce output, such as informational messages, error messages, the requested data, or result of the operation.
7. The CLI shell displays the output, so you can see the outcome of the command.

Some basic commands used in Linux CLI:

- `cd` - Change directory.
- `ls` - list sub-directories and files (can be used with various flags as per the output requirement).
- `mkdir directory_name` - Creates a directory.
- `rmdir directory_name` - Removes a directory (only if the directory is empty otherwise -rf flag is required).
- `touch file_name.ext` - Creates an empty file.
- `man command_name` - Manual for the specified command.

For more Linux CLI command, checkout [Top 50 Linux Commands](https://www.digitalocean.com/community/tutorials/linux-commands)